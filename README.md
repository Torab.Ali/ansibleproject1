**Ansible Playbook for Server Provisioning using Handlers (Branch: Master)**

This Ansible playbook automates the process of provisioning servers by installing and configuring necessary packages and services based on the server's operating system distribution. Additionally, it ensures consistent server configuration by deploying configuration files and managing service states.

**Project Structure**

**playbook.yml:** This is the main Ansible playbook file that defines tasks for provisioning servers.

**group_vars:** This folder contains variables used in the project, such as dir1.

**ansible.cfg:** Ansible configuration file.

**hostfile:** Inventory file containing details of target servers.

**Usage**

**Clone the Repository:** Clone this repository to your local machine using Git.

**Update Inventory:** Modify the hostfile to include details of your target servers, such as IP addresses or hostnames.

**Adjust Variables (Optional):** Update variables in the group_vars folder to customize configurations for your environment.

**Run the Playbook:** Execute the playbook using the ansible-playbook command, specifying the playbook.yml file and inventory file.

```
**bash**:
ansible-playbook -i hostfile playbook.yml
```


**Features**

**Operating System Detection:** Tasks are conditionally executed based on the server's operating system distribution (CentOS or Ubuntu).

**Package Installation:** Installs necessary packages such as wget, git, zip, unzip, and ntp or chrony.

**Service Management:** Ensures services (ntp or chronyd) are started and enabled.

**Configuration Deployment:** Deploys configuration files (ntp.conf or chrony.conf) based on the server's distribution.
**Motd Message:** Sets a banner message in /etc/motd indicating that the server is managed by Ansible.

**Requirements**

Ansible installed on the control node.

Target servers accessible via SSH and listed in the inventory file (hostfile).

Properly configured network connectivity between the control node and target servers.

**License**

This project is licensed under the MIT License.